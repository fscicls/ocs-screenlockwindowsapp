using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Runtime.InteropServices;
using System.Threading;

namespace Keyhook
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form2 : System.Windows.Forms.Form
	{

		private delegate int LowLevelKeyboardProcDelegate(int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam);
		[DllImport( "user32.dll", EntryPoint="SetWindowsHookExA", CharSet=CharSet.Ansi)]
		private static extern int SetWindowsHookEx(int idHook, LowLevelKeyboardProcDelegate lpfn, int hMod , int dwThreadId);
		[DllImport( "user32.dll", EntryPoint="UnhookWindowsHookEx", CharSet=CharSet.Ansi)]
		private static extern int UnHookWindowsEx(int hHook);
		[DllImport("user32.dll", EntryPoint="CallNextHookEx", CharSet=CharSet.Ansi)]
		private static extern int CallNextHookEx(int hHook, int nCode, int wParam, ref KBDLLHOOKSTRUCT lParam);

		const int WH_KEYBOARD_LL = 13;

		public struct KBDLLHOOKSTRUCT
		{
			public int vkCode;
			int scanCode;
			public int flags;
			int time;
			int dwExtraInfo;
		}

		private int intLLKey;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox1;
		private KBDLLHOOKSTRUCT lParam; 

		string temp;
		private int LowLevelKeyboardProc(int nCode,int wParam, ref KBDLLHOOKSTRUCT lParam)
		{
			bool blnEat = false;
						switch (wParam)
						{
							case 256:
							case 257:
							case 260:
							case 261:
			//Alt+Tab, Alt+Esc, Ctrl+Esc, Windows Key
			if (((lParam.vkCode == 27) && (lParam.flags == 0)) || 
				((lParam.vkCode == 91) && (lParam.flags == 1)) || 
				((lParam.vkCode == 92) && (lParam.flags == 1)) || 
				(lParam.flags == 32))
			{
				blnEat = true;
			} 
								break;
						}
			if (blnEat)
				return 1;
			else
			{ 
				label1.Text += CallNextHookEx(0, nCode, wParam, ref lParam).ToString();
				return CallNextHookEx(0, nCode, wParam, ref lParam);
			}
		}

		public void KeyboardHook()
		{ 
			intLLKey = SetWindowsHookEx(WH_KEYBOARD_LL,
				new LowLevelKeyboardProcDelegate(LowLevelKeyboardProc), System.Runtime.InteropServices.Marshal.GetHINSTANCE(System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0]).ToInt32(), 0); 
		}

		public void KeyBoardUnHook()
		{
			UnHookWindowsEx(intLLKey); 
		}


		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public Form2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(184, 72);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(280, 72);
			this.button2.Name = "button2";
			this.button2.TabIndex = 1;
			this.button2.Text = "button2";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(104, 104);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(352, 104);
			this.label1.TabIndex = 2;
			this.label1.Text = "label1";
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(48, 32);
			this.textBox1.Name = "textBox1";
			this.textBox1.TabIndex = 3;
			this.textBox1.Text = "textBox1";
			// 
			// Form2
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(536, 266);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Form2";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form2());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			GC.KeepAlive(this);
			Monitor.Enter(this);
	

			try
			{
				// set the hook here...
				KeyboardHook();
			}
			finally
			{
				Monitor.Exit(this);
			} 
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
//			Form1 test = new Form1();
//			test.Show();
			label1.Text = temp;
		}
	}
}
