using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Timers;
using System.Management;
using System.Net;


using ClassLibrary;
using UserInactivityMonitoring;
using Microsoft.Win32;
using MySql.Data.MySqlClient;
using CustomUIControls;

//for encryption
using System.IO;
using System.Text;
using System.Security;
using System.Security.Cryptography;


namespace Main1Window
{
	/// <summary>
	/// Summary description for Login.
	/// </summary>
	public class Login : System.Windows.Forms.Form
	{

		#region Initial Program

		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox pass;
		private System.Windows.Forms.TextBox username;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.ComponentModel.IContainer components;


		private System.Windows.Forms.Timer timer1;
		private Hook key = new Hook();
		bool showWarning;
		private System.Windows.Forms.Timer timerIDLE;

		private UserProfile user = new UserProfile();
		

		//Monitor inactivity mouse and keyboard
		private IInactivityMonitor inactivityMonitor = null;
		private System.Windows.Forms.Label CompName;

		//IMAP
		private Imap oImap = new Imap();

		//Pop up show warning
		TaskbarNotifier taskbarNotifier1;
		private System.Windows.Forms.Timer timer2;

		//Debuging
		private bool debug;

		public Login()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();


			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			taskbarNotifier1 = new TaskbarNotifier();
			taskbarNotifier1.SetBackgroundBitmap(new Bitmap(GetType(),"skin3.bmp"),Color.FromArgb(255,0,255));
			taskbarNotifier1.SetCloseBitmap(new Bitmap(GetType(),"close.bmp"),Color.FromArgb(255,0,255),new Point(280,57));
			taskbarNotifier1.TitleRectangle=new Rectangle(150, 57, 125, 28);
			taskbarNotifier1.ContentRectangle=new Rectangle(75, 92, 215, 55);
			taskbarNotifier1.TitleClick+=new EventHandler(TitleClick);
			taskbarNotifier1.ContentClick+=new EventHandler(ContentClick);
			taskbarNotifier1.CloseClick+=new EventHandler(CloseClick);

			debug = false;

			timer2.Enabled = true;
			this.AcceptButton = button1;
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#endregion

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Login));
			this.label5 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.pass = new System.Windows.Forms.TextBox();
			this.username = new System.Windows.Forms.TextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.timerIDLE = new System.Windows.Forms.Timer(this.components);
			this.CompName = new System.Windows.Forms.Label();
			this.timer2 = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			this.label5.ForeColor = System.Drawing.Color.Black;
			this.label5.Location = new System.Drawing.Point(320, 710);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(384, 40);
			this.label5.TabIndex = 14;
			this.label5.Text = "�к����׹�ѹ��èͧ����ѵ��ѵ���ѧ�ҡ Login\r\n�����ͤ˹�Ҩ��ѵ��ѵ���������������" +
				"������ա����ҹ 5 �ҷ�";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			this.label3.Location = new System.Drawing.Point(300, 620);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(420, 20);
			this.label3.TabIndex = 13;
			this.label3.Text = "��س� Login ���� Username ��� Password ���͢��¹����";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			this.label2.Location = new System.Drawing.Point(390, 680);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(70, 16);
			this.label2.TabIndex = 12;
			this.label2.Text = "Password";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			this.label1.Location = new System.Drawing.Point(390, 650);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(70, 16);
			this.label1.TabIndex = 11;
			this.label1.Text = "Username";
			// 
			// pass
			// 
			this.pass.Location = new System.Drawing.Point(460, 680);
			this.pass.Name = "pass";
			this.pass.PasswordChar = '*';
			this.pass.TabIndex = 2;
			this.pass.Text = "";
			// 
			// username
			// 
			this.username.Location = new System.Drawing.Point(460, 650);
			this.username.MaxLength = 8;
			this.username.Name = "username";
			this.username.TabIndex = 1;
			this.username.Text = "";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(580, 680);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(72, 23);
			this.button1.TabIndex = 3;
			this.button1.Text = "Login";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.Transparent;
			this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
			this.panel1.Location = new System.Drawing.Point(290, 575);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(440, 190);
			this.panel1.TabIndex = 15;
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.ContextMenu = this.contextMenu1;
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "Logout Here";
			this.notifyIcon1.Visible = true;
			this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.menuItem2,
																						 this.menuItem1});
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "Log Out";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 1;
			this.menuItem1.Text = "Exit";
			this.menuItem1.Visible = false;
			this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
			// 
			// timer1
			// 
			this.timer1.Interval = 59500;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// timerIDLE
			// 
			this.timerIDLE.Interval = 300000;
			this.timerIDLE.Tick += new System.EventHandler(this.timerIDLE_Tick);
			// 
			// CompName
			// 
			this.CompName.BackColor = System.Drawing.Color.Transparent;
			this.CompName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			this.CompName.ForeColor = System.Drawing.Color.Brown;
			this.CompName.Location = new System.Drawing.Point(850, 40);
			this.CompName.Name = "CompName";
			this.CompName.Size = new System.Drawing.Size(100, 70);
			this.CompName.TabIndex = 17;
			this.CompName.Text = "Com ID";
			this.CompName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// timer2
			// 
			this.timer2.Interval = 59500;
			this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
			// 
			// Login
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(1024, 768);
			this.Controls.Add(this.CompName);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.pass);
			this.Controls.Add(this.username);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.panel1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Login";
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "Login";
			this.TopMost = true;
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.Closing += new System.ComponentModel.CancelEventHandler(this.Login_Closing);
			this.Load += new System.EventHandler(this.Login_Load);
			this.ResumeLayout(false);

		}
		#endregion

		#region Main Program
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Login());
		}
		#endregion

		#region .Net function

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			notifyIcon1.Visible = false;
			Application.Exit();
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			if(CheckInput() == 1)
			{	
				if(username.Text == "valta")
				{
					Unlock();
					user.userprofile("admin");
				}
				else if(username.Text == "admin2")
				{
					if(pass.Text == Password("admin"))
					{
						Unlock();
						user.userprofile("admin");
					}
					else
					{
						pass.Text = "";				
						label5.Text = "* Check your username and password";
						if(debug)
							label5.Text += " error:1";
						label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
						label5.ForeColor = System.Drawing.Color.IndianRed;
					}
				}
				else if(username.Text == "exit2")
				{
					if(pass.Text == Password("exit"))
					{
						notifyIcon1.Visible = false;
						Unlock();
						Application.Exit();
					}
					else
					{
						pass.Text = "";
						label5.Text = "* Check your username and password";
						if(debug)
							label5.Text += " error:2";
						label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
						label5.ForeColor = System.Drawing.Color.IndianRed;
					}
				}
				else if(username.Text == "admin1")
				{
					if(PassEncrypt(username.Text,pass.Text) == 0)
					{
						Unlock();
						user.userprofile("admin");
					}
					else
					{
						pass.Text = "";				
						label5.Text = "* Check your username and password";
						
						if(debug)
							label5.Text += " error:1";
						label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
						label5.ForeColor = System.Drawing.Color.IndianRed;
					}
				}
				else if(username.Text == "exit1")
				{
					if(PassEncrypt(username.Text,pass.Text) == 0)
					{
						notifyIcon1.Visible = false;
						Unlock();
						Application.Exit();
					}
					else
					{
						pass.Text = "";
						label5.Text = "* Check your username and password";
						if(debug)
							label5.Text += " error:2";
						label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
						label5.ForeColor = System.Drawing.Color.IndianRed;
					}
				}
				else if(Char.IsLetter(username.Text,0))
				{
					if(user.comid == "Can't connect to Database")
					{
						user.CheckComID();
						CompName.Text = user.comid;
						if(user.internetState == 0)
						{
							pass.Text = "";
							label5.Text = "* No Network connection Please Contact Admin";
							if(debug)
								label5.Text += " error:5";
							label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
							label5.ForeColor = System.Drawing.Color.IndianRed;
							return;
						}
					}
					int tmplogin = oImap.Login("nontri.ku.ac.th", username.Text , "\"" + pass.Text + "\"");
					int tmpstatus;
					if(tmplogin == 1)
					{
						user.userprofile(username.Text);
						//						MessageBox.Show("date = "+user.day + user.month + user.year + "\r\n" +
						//							"time = " + user.period_time);

						Daytime tmpnow = new Daytime();
						DateTime tmpDateTime = new DateTime();
						tmpstatus = user.CheckTime(tmpnow.GetTime());
						if(tmpstatus == 1)
						{
							user.Confirm();
							Unlock();
						}
						else if(tmpstatus == -1)
						{
							pass.Text = "";
							label5.Text = "* ��ҹ��觼Դ����ͧ ����ͧ���ͧ��������Ţ " + user.machine_id ;
							if(debug)
								label5.Text += " error:3";
							label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
							label5.ForeColor = System.Drawing.Color.IndianRed;
						}
						else
						{
							tmpDateTime = tmpnow.GetTime();
							pass.Text = "";
							label5.Text = "* �ͧ���� " + user.period_time +".30 - " +
								(user.period_time + 1) + ".30 �����Ţ����ͧ " + user.machine_id +
								" " + user.day + "/" + user.month + "/" + user.year + "\n��й�� " + 
								tmpDateTime.ToShortTimeString() + " �ѹ��� " + tmpDateTime.ToShortDateString();
							if(debug)
								label5.Text += " error:4";
							label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
							label5.ForeColor = System.Drawing.Color.IndianRed;
						}
					}
					else if(tmplogin == -1)
					{
						pass.Text = "";
						label5.Text = "* No Network connection Please Contact Admin";
						if(debug)
							label5.Text += " error:5";
						label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
						label5.ForeColor = System.Drawing.Color.IndianRed;
					}
					else
					{
						pass.Text = "";
						label5.Text = "* Check your Username and Password";
						if(debug)
							label5.Text += " error:6";
						label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
						label5.ForeColor = System.Drawing.Color.IndianRed;
					}
				}
				else
				{
					label5.Text = "* Invalid Username";
					if(debug)
						label5.Text += " error:7";
					label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
					label5.ForeColor = System.Drawing.Color.IndianRed;
				}
			}
			else
			{
				label5.Text = "* Check your username and password";
				if(debug)
					label5.Text += " error:8";
				label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
				label5.ForeColor = System.Drawing.Color.IndianRed;
			}
			
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			Lock();
		}

		private void Login_Load(object sender, System.EventArgs e)
		{
			startMonitor();
			Lock();			
			CompName.Text = user.comid;
			if(HideOnline() == 1)
			{
				Unlock();
				user.userprofile("hide");
			}
			try
			{
				RegistryKey rk = Registry.LocalMachine;
				RegistryKey sk = rk.CreateSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\");
				sk.SetValue("KITS", Application.ExecutablePath.ToString());
			}
			catch
			{
				//Don't show in real version
				//MessageBox.Show("Can't create registry!");
				//
			}
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			if((user.std_id == "hide") && (HideOnline() == 1))
			{
				return;
			}
			else if(user.std_id == "admin")
			{
			}
			else
			{
				Daytime tmpnow = new Daytime();
				DateTime now = tmpnow.GetTime();

				if(user.CheckTime(now) == 0)
					Lock();
				else if(((now.Minute == 15) || (now.Minute == 25) || (now.Minute == 29)) && showWarning)
					ShowTaskbarNotifier("���ҡ����ҹ","��ҹ����������ա " + (30-Convert.ToInt16(now.Minute)) + " �ҷ� \r\n�Ѵ�红�����������º���¡�͹������ҡ����ҹ");
			}
			
		}

		private void notifyIcon1_DoubleClick(object sender, System.EventArgs e)
		{
			Lock();
		}


		
		#endregion

		#region General Function
		/*All offline function*/
		
		/*Lock screen and set variable*/
		public void Unlock()
		{			
			this.WindowState = FormWindowState.Minimized;
			this.Hide();
			key.KeyBoardUnHook();
			showWarning = true;
			timer1.Enabled = true;
			timer2.Enabled = false;
			inactivityMonitor.Enabled = true;

		}
		/*Lock screen and set variable*/
		public void Lock()
		{
			UpdatePicture();
			label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(222)));
			label5.ForeColor = System.Drawing.Color.Black;
			label5.Text = "�к����׹�ѹ��èͧ����ѵ��ѵ���ѧ�ҡ Login\r\n�����ͤ˹�Ҩ��ѵ��ѵ���������������" +
				"������ա����ҹ 5 �ҷ�";
			
			this.WindowState = FormWindowState.Maximized;
			this.Show();
			key.KeyBoardHook();
			showWarning = false;
			timer1.Enabled = false;
			timer2.Enabled = true;
			username.Text = "";
			pass.Text = "";
			inactivityMonitor.Enabled = false;
		}

		/*Input not empty*/
		private int CheckInput()
		{
			if((username.Text != "") && (pass.Text != ""))
			{
				return 1;
			}
			return 0;
		}
		
		/*Check password by file in stand alone PC(use for no connection to server)*/
		private int PassEncrypt(string inputUsername, string inputPassword)
		{
			string strComp1;
			string strComp2;

			string NextLine = inputPassword;
			string fileName = inputUsername;

			byte[] data = new byte[18]; 
			byte[] result; 
			SHA512 shaM2 = new SHA512Managed(); 
			result = shaM2.ComputeHash(ASCIIEncoding.ASCII.GetBytes(NextLine));

			strComp1 = ASCIIEncoding.ASCII.GetString(result);
			
			byte[] input = new byte[512];
			string path;
			path = Application.StartupPath + "\\" + fileName;

			FileStream fs = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Read);
			fs.Read(input,0,512);
			fs.Close();

			strComp2 = ASCIIEncoding.ASCII.GetString(input);

			
			

			if(String.Compare(strComp1,strComp2) == 0)
				return 0;
			else
				return -1;
		}

		#endregion

		#region IDLE Funtion
		private void startMonitor()
		{
			inactivityMonitor = MonitorCreator.CreateInstance(MonitorType.GlobalHookMonitor);
			inactivityMonitor.SynchronizingObject = this;
			inactivityMonitor.MonitorKeyboardEvents = true;
			inactivityMonitor.MonitorMouseEvents = true;
			//Time wait 
			inactivityMonitor.Interval = 5000;

			inactivityMonitor.Elapsed += new ElapsedEventHandler(TimeElapsed);
			inactivityMonitor.Reactivated += new EventHandler(Reactivated);
//			inactivityMonitor.Enabled = true;
//			MessageBox.Show("Monitor enabled");
		}

		private void TimeElapsed(object sender, ElapsedEventArgs e)
		{
			timerIDLE.Enabled = true;
			timerIDLE.Start();
		}

		private void Reactivated(object sender, EventArgs e)
		{			
			timerIDLE.Enabled = false;
			timerIDLE.Stop();
		}

		private void timerIDLE_Tick(object sender, System.EventArgs e)
		{
			if(user.std_id != "hide")
			{
				Lock();
				timerIDLE.Enabled =false;
				timerIDLE.Stop();
			}
		}
		#endregion

		#region Online_Function
		
		private string Password(string strUser)
		/*Check password to server admin2 & exit2*/
		{
			MySqlConnection		conn = null;
			DataTable			data = new DataTable();
			MySqlDataAdapter	da = null;
			MySqlCommandBuilder	cb = null;
			
			/*Real Server*/
			string connStr = String.Format("server={0};user id={1}; password={2}; pooling=false",
				DATABASE.ADDRESS, DATABASE.USERNAME, DATABASE.PASSWORD );


			if (conn != null)
				conn.Close();
			
			try 
			{
				/*connect to database KITS*/
				conn = new MySqlConnection( connStr );
				conn.Open();
				conn.ChangeDatabase(DATABASE.TABLE);

				/*SQL query*/
				da = new MySqlDataAdapter("SELECT * " +
										  "FROM `user` " +
										  "WHERE `user` LIKE '"+ strUser +"'", conn );

				
				cb = new MySqlCommandBuilder( da );
				da.Fill( data );
				user.internetState = 1;
			}
			catch
			{
				user.internetState = 0;
			}
			if(data.Rows.Count != 0)
			{
				Console.WriteLine(data.Rows[0]["pass"].ToString());
				return data.Rows[0]["pass"].ToString();
			}
			return "";
		}


		private int HideOnline()
		/*Hide via Webbrowser*/
		{
			/*No internet connection*/
			if(user.internetState == 0)
				return 0;

			MySqlConnection		conn = null;
			DataTable			data = new DataTable();
			MySqlDataAdapter	da = null;
			MySqlCommandBuilder	cb = null;

			/*Real Server*/
			string connStr = String.Format("server={0};user id={1}; password={2}; pooling=false",
				DATABASE.ADDRESS, DATABASE.USERNAME, DATABASE.PASSWORD );  

			if (conn != null)
				conn.Close();
			
			try 
			{
				/*connect to database KITS*/
				conn = new MySqlConnection( connStr );
				conn.Open();
				conn.ChangeDatabase(DATABASE.TABLE);

				/*SQL query*/
				da = new MySqlDataAdapter("SELECT * " +
					"FROM `control` " +
					"WHERE `name` LIKE 'hide'", conn );

				cb = new MySqlCommandBuilder( da );
				da.Fill( data );
				user.internetState = 1;
			}
			catch
			{
				user.internetState = 0;
			}
			if(data.Rows.Count != 0)
			{
//				Console.WriteLine(data.Rows[0]["value"].ToString());
				return Convert.ToInt32(data.Rows[0]["value"]);
			}
			return 0;
		}

		
		private void UpdatePicture()
		/*Update Picture from server*/
		{
			/*No internet connection*/
			if(user.internetState == 0)
				return;

			MySqlConnection		conn = null;
			DataTable			data = new DataTable();
			MySqlDataAdapter	da = null;
			MySqlCommandBuilder	cb = null;

			string connStr = String.Format("server={0};user id={1}; password={2}; pooling=false",
				DATABASE.ADDRESS, DATABASE.USERNAME, DATABASE.PASSWORD );  //Real Server
			//			"localhost", "root", "" );  //Localhost


			if (conn != null)
				conn.Close();
	
			
			try 
			{
				//connect to database KITS
				conn = new MySqlConnection( connStr );
				conn.Open();
				conn.ChangeDatabase(DATABASE.TABLE);

				
				da = new MySqlDataAdapter("SELECT * " +
					"FROM `Picture` " +
					"WHERE `set` = 1", conn );

				
				cb = new MySqlCommandBuilder( da );
				da.Fill( data );
				user.internetState = 1;
			}
			catch
			{
				user.internetState = 0;
			}
			if(data.Rows.Count != 0)
			{
				string remoteUri = data.Rows[0]["pic_path"].ToString();
				string fileName = data.Rows[0]["pic_name"].ToString();
				string myStringWebResource = null;
			
				// Create a new WebClient instance.
				WebClient myWebClient = new WebClient();

				try
				{
					// Concatenate the domain with the Web resource filename.
					myStringWebResource = remoteUri + fileName;

//					Console.WriteLine("Downloading File \"{0}\" from \"{1}\" .......\n\n", fileName, myStringWebResource);

					// Download the Web resource and save it into the current filesystem folder.
					myWebClient.DownloadFile(myStringWebResource,fileName);

//					Console.WriteLine("Successfully Downloaded File \"{0}\" from \"{1}\"", fileName, myStringWebResource);
//					Console.WriteLine("\nDownloaded file saved in the following file system folder:\n\t" + Application.StartupPath); 
					
					this.BackgroundImage = new Bitmap(@fileName);
				}
				catch
				{
					Console.WriteLine("Can't find pic");
				}
			}
		}

		private int ExitOnline()
			/*Exit via Webbrowser*/
		{
			/*No internet connection*/
			if(user.internetState == 0)
				return 0;

			MySqlConnection		conn = null;
			DataTable			data = new DataTable();
			MySqlDataAdapter	da = null;
			MySqlCommandBuilder	cb = null;

			/*Real Server*/
			string connStr = String.Format("server={0};user id={1}; password={2}; pooling=false",
				DATABASE.ADDRESS, DATABASE.USERNAME, DATABASE.PASSWORD );  

			if (conn != null)
				conn.Close();
			
			try 
			{
				/*connect to database KITS*/
				conn = new MySqlConnection( connStr );
				conn.Open();
				conn.ChangeDatabase(DATABASE.TABLE);

				/*SQL query*/
				da = new MySqlDataAdapter("SELECT * " +
					"FROM `control` " +
					"WHERE `name` LIKE 'shutdown'", conn );

				cb = new MySqlCommandBuilder( da );
				da.Fill( data );
				user.internetState = 1;
			}
			catch
			{
				user.internetState = 0;
			}
			if(data.Rows.Count != 0)
			{
				//				Console.WriteLine(data.Rows[0]["value"].ToString());
				return Convert.ToInt32(data.Rows[0]["value"]);
			}
			return 0;
		}
		
		
		#endregion

		#region ShowTaskbarNotifier Funtion
		private void ShowTaskbarNotifier(string strTitle,string strContent)
		{
			taskbarNotifier1.CloseClickable=true;
			taskbarNotifier1.TitleClickable=false;
			taskbarNotifier1.ContentClickable=false;
			taskbarNotifier1.EnableSelectionRectangle=true;
			taskbarNotifier1.KeepVisibleOnMousOver=true;	// Added Rev 002
			taskbarNotifier1.ReShowOnMouseOver=true;			// Added Rev 002
			taskbarNotifier1.Show(strTitle,strContent,500,5000,500);
		}

		
		
		void CloseClick(object obj,EventArgs ea)
		{
		//	MessageBox.Show("Closed was Clicked");
		}

		void TitleClick(object obj,EventArgs ea)
		{
		//	MessageBox.Show("Title was Clicked");
		}

		void ContentClick(object obj,EventArgs ea)
		{
		//	MessageBox.Show("Content was Clicked");
		}
		#endregion

		private void Login_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			e.Cancel = true;
		}

		private void timer2_Tick(object sender, System.EventArgs e)
		{
			if(HideOnline() == 1)
			{
				Unlock();
				user.userprofile("hide");
			}
			else if(ExitOnline() == 1)
			{
				notifyIcon1.Visible = false;
				Unlock();
				Application.Exit();
			}
		}
	}
}
