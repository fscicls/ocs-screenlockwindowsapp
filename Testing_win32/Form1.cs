using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using ClassLibrary;

namespace Testing_win32
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Label showtime;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.NotifyIcon notifyIcon1;
		private System.Windows.Forms.ContextMenu contextMenu1;
		private System.Windows.Forms.MenuItem menuItem1;
		private System.Windows.Forms.MenuItem menuItem2;
		private System.Windows.Forms.MenuItem menuItem3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.MenuItem menuItem4;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button8;
		private System.ComponentModel.IContainer components;
		private System.Windows.Forms.Button button9;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.TextBox textBox2;
		private System.Windows.Forms.Button button10;
		private System.Windows.Forms.Timer timer1;

		private Hook gogogo = new Hook();

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.showtime = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.label1 = new System.Windows.Forms.Label();
			this.button6 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button9 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.textBox2 = new System.Windows.Forms.TextBox();
			this.button10 = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(456, 24);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "KillTaskMng";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(552, 72);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(128, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "ShowTaskBar(false)";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(456, 112);
			this.button3.Name = "button3";
			this.button3.TabIndex = 2;
			this.button3.Text = "DisableTaskMgr";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// showtime
			// 
			this.showtime.Location = new System.Drawing.Point(112, 32);
			this.showtime.Name = "showtime";
			this.showtime.Size = new System.Drawing.Size(208, 24);
			this.showtime.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 32);
			this.label2.Name = "label2";
			this.label2.TabIndex = 4;
			this.label2.Text = "this time";
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(328, 32);
			this.button4.Name = "button4";
			this.button4.TabIndex = 5;
			this.button4.Text = "this time";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(552, 24);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(128, 23);
			this.button5.TabIndex = 6;
			this.button5.Text = "setCaltofront";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// notifyIcon1
			// 
			this.notifyIcon1.ContextMenu = this.contextMenu1;
			this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
			this.notifyIcon1.Text = "notifyIcon1";
			this.notifyIcon1.Visible = true;
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.menuItem1,
																						 this.menuItem2,
																						 this.menuItem3,
																						 this.menuItem4});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "Hide";
			this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "Show";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 2;
			this.menuItem3.Text = "test3";
			this.menuItem3.Click += new System.EventHandler(this.menuItem3_Click);
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 3;
			this.menuItem4.Text = "exit";
			this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(32, 168);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(176, 72);
			this.label1.TabIndex = 7;
			this.label1.Text = "label1";
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(552, 112);
			this.button6.Name = "button6";
			this.button6.TabIndex = 8;
			this.button6.Text = "EnableTask";
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// button7
			// 
			this.button7.Location = new System.Drawing.Point(448, 72);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(80, 23);
			this.button7.TabIndex = 9;
			this.button7.Text = "HideTaskBar";
			this.button7.Click += new System.EventHandler(this.button7_Click);
			// 
			// button8
			// 
			this.button8.Location = new System.Drawing.Point(456, 152);
			this.button8.Name = "button8";
			this.button8.TabIndex = 10;
			this.button8.Text = "UnHook";
			this.button8.Click += new System.EventHandler(this.button8_Click);
			// 
			// button9
			// 
			this.button9.Location = new System.Drawing.Point(552, 152);
			this.button9.Name = "button9";
			this.button9.TabIndex = 11;
			this.button9.Text = "Hook";
			this.button9.Click += new System.EventHandler(this.button9_Click);
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(248, 184);
			this.textBox1.Name = "textBox1";
			this.textBox1.TabIndex = 12;
			this.textBox1.Text = "textBox1";
			// 
			// textBox2
			// 
			this.textBox2.Location = new System.Drawing.Point(248, 224);
			this.textBox2.Name = "textBox2";
			this.textBox2.TabIndex = 13;
			this.textBox2.Text = "textBox2";
			// 
			// button10
			// 
			this.button10.Location = new System.Drawing.Point(280, 264);
			this.button10.Name = "button10";
			this.button10.TabIndex = 14;
			this.button10.Text = "submit";
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(792, 494);
			this.Controls.Add(this.button10);
			this.Controls.Add(this.textBox2);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.button9);
			this.Controls.Add(this.button8);
			this.Controls.Add(this.button7);
			this.Controls.Add(this.button6);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button5);
			this.Controls.Add(this.button4);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.showtime);
			this.Controls.Add(this.button3);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.ShowInTaskbar = false;
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			_Win32 test = new _Win32();
			test.KillTaskMng();

            test.ShowTaskBar(true);			
		}

		private void button2_Click(object sender, System.EventArgs e)
		{
			_Win32 test = new _Win32();
			test.ShowTaskBar(true);		
		}

		private void button3_Click(object sender, System.EventArgs e)
		{
			_Win32 test = new _Win32();
			test.DisableTaskMgr(true);
		}

		private void button4_Click(object sender, System.EventArgs e)
		{
			DateTime now = DateTime.Now;
			showtime.Text = now.ToLongTimeString();
		}

		private void button5_Click(object sender, System.EventArgs e)
		{
			_Win32 test = new _Win32();
			test.ShowThisFront("SciCalc","Calculator");
		}

		private void menuItem1_Click(object sender, System.EventArgs e)
		{
			this.WindowState = FormWindowState.Minimized;
			Hide();
		}

		private void menuItem2_Click(object sender, System.EventArgs e)
		{
			Show();
			this.WindowState = FormWindowState.Normal;
		}

		private void menuItem3_Click(object sender, System.EventArgs e)
		{
			label1.Text = "TEST3";
		}

		private void menuItem4_Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}

		private void button6_Click(object sender, System.EventArgs e)
		{
		
			_Win32 test = new _Win32();
			test.DisableTaskMgr(false);
		}

		private void button7_Click(object sender, System.EventArgs e)
		{
			_Win32 test = new _Win32();
			test.ShowTaskBar(false);
		}

		private void Form1_Load(object sender, System.EventArgs e)
		{
			
			gogogo.KeyBoardHook();
		}

		private void button8_Click(object sender, System.EventArgs e)
		{
			gogogo.KeyBoardUnHook();
		}

		private void button9_Click(object sender, System.EventArgs e)
		{
			gogogo.KeyBoardHook();
		}

		private void timer1_Tick(object sender, System.EventArgs e)
		{
			DateTime now = DateTime.Now;
			showtime.Text = now.Minute.ToString();
		}
	}
}
