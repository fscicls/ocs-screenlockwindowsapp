using System;
using System.Runtime.InteropServices;


//don't forget to change Namespace
namespace ClassLibrary
{
	/// <summary>
	/// Summary description for Hook.
	/// </summary>
	public class Hook
	{
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int Desktop_Show_Hide(bool bShowHide);
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int StartButton_Show_Hide(bool bShowHide);
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int Taskbar_Show_Hide(bool bShowHide);
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int Clock_Show_Hide(bool bShowHide);
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int Process_Desktop(string szDesktopName, string szPath);
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int Keys_Enable_Disable(bool bEnableDisable);
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int AltTab1_Enable_Disable(bool bEnableDisable);
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int AltTab2_Enable_Disable(long hWnd, bool bEnableDisable);
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int TaskSwitching_Enable_Disable(bool bEnableDisable);
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int TaskManager_Enable_Disable(bool bEnableDisable);
		[System.Runtime.InteropServices.DllImport("WinLockDll.dll")]
		private static extern int CtrlAltDel_Enable_Disable(bool bEnableDisable);

		public void KeyBoardHook()
		{ 
//			Desktop_Show_Hide(false);
//			StartButton_Show_Hide(false);
			Taskbar_Show_Hide(false);
			Keys_Enable_Disable(false);
//			AltTab1_Enable_Disable(false);
//			AltTab2_Enable_Disable(0, false);
			TaskSwitching_Enable_Disable(false);
//			TaskManager_Enable_Disable(false);
			CtrlAltDel_Enable_Disable(false);
		}

		public void KeyBoardUnHook()
		{
//			Desktop_Show_Hide(true);
//			StartButton_Show_Hide(true);
			Taskbar_Show_Hide(true);
			Keys_Enable_Disable(true);
//			AltTab1_Enable_Disable(true);
//			AltTab2_Enable_Disable(0, true);
			TaskSwitching_Enable_Disable(true);
//			TaskManager_Enable_Disable(true);
			CtrlAltDel_Enable_Disable(true);
		}

		public Hook()
		{
		}
	}
}
