using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Management;

namespace ClassLibrary
{
	/// <summary>
	/// Summary description for DataBaseProfile.
	/// </summary>
	public class UserProfile
	{
		public int internetState = 0;
		private MySqlConnection		conn;
		private DataTable			data;
		private MySqlDataAdapter	da;
		private MySqlCommandBuilder	cb;

		

		private string connStr = String.Format("server={0};user id={1}; password={2}; pooling=false",
			DATABASE.ADDRESS, DATABASE.USERNAME, DATABASE.PASSWORD );  //Real Server
//			"localhost", "root", "" );  //Localhost

		public string std_id = null;		//Column 0
		public string machine_id;	//Column 3
		public int period_time;		//Column 4
		public int day;				//Column 5
		public int month;			//Column 6
		public int year;			//Column 7
		public int status;			//Column 9
		
		public string comid;

		public string error;

		public UserProfile()
		{
			CheckComID();				
		}

		public void CheckComID()
		{

			if (conn != null)
				conn.Close();
	
			try
			{
				conn = new MySqlConnection( connStr );
				conn.Open();
				conn.ChangeDatabase(DATABASE.TABLE);

				data = new DataTable();

				da = new MySqlDataAdapter("SELECT * "+
					"FROM `comp_name` " +
					"WHERE " + GetMAC() , conn );
				cb = new MySqlCommandBuilder( da );
				da.Fill( data );
				if(data.Rows.Count != 0)
					comid = data.Rows[0][0].ToString();
				else
					comid = "Not Found in Database";
				internetState = 1;
			}
			catch
			{
				internetState = 0;
				comid = "Can't connect to Database";
			}
		}


		public void userprofile(string UserName)
		{
			//
			// TODO: Add constructor logic here
			//
			std_id = "";
			machine_id= "";
			period_time= -1;
			day = -1;
			month = -1;
			year = -1;
			status = 0;
			
			
			if((UserName == "admin") || (UserName == "hide"))
			{
				std_id = UserName;
				return;
			}
			
			
			if (conn != null)
				conn.Close();
	
			//			string connStr = String.Format("server={0};user id={1}; password={2}; database=mysql; pooling=false",
			
			try 
			{
				//connect to database KITS
				conn = new MySqlConnection( connStr );
				conn.Open();
				conn.ChangeDatabase(DATABASE.TABLE);

				data = new DataTable();

				Daytime tmpnow = new Daytime();
				DateTime now = tmpnow.GetTime();


				if(now.Minute > 30)
				{
					da = new MySqlDataAdapter("SELECT * "+
						"FROM `comp_reserve` " +
						"WHERE `std_id` = '" + UserName + "' " +
						"AND `day` = " + now.Day +
						" AND `month` = " + now.Month +
						" AND `year` = "+ now.Year +
						" AND `period_time` = " + now.Hour , conn );
				}
				else
				{
					da = new MySqlDataAdapter("SELECT * "+
						"FROM `comp_reserve` " +
						"WHERE `std_id` = '" + UserName + "' " +
						"AND `day` = " + now.Day +
						" AND `month` = " + now.Month +
						" AND `year` = "+ now.Year +
						" AND `period_time` = " + (now.Hour-1) , conn );
				}
			

				cb = new MySqlCommandBuilder( da );
				da.Fill( data );

				if(data.Rows.Count != 0)
				{
					std_id = data.Rows[0][0].ToString();					
					machine_id= data.Rows[0][3].ToString();	
					if(data.Rows[0][4] != null)
						period_time= Convert.ToInt16(data.Rows[0][4]);	
					if(data.Rows[0][5] != null)
						day= Convert.ToInt16(data.Rows[0][5]);			
					if(data.Rows[0][6] != null)
						month= Convert.ToInt16(data.Rows[0][6]);		
					if(data.Rows[0][7] != null)
						year= Convert.ToInt16(data.Rows[0][7]);			
					if(data.Rows[0][9] != null)
						status= Convert.ToInt16(data.Rows[0][9]);		
				}
				else
				{
					da = new MySqlDataAdapter("SELECT * "+
						"FROM `comp_reserve` " +
						"WHERE `std_id` = '" + UserName + "' " +
						"ORDER BY `timestamp` DESC ", conn );
				

					cb = new MySqlCommandBuilder( da );

					da.Fill( data );

					if(data.Rows.Count != 0)
					{
						std_id = data.Rows[0][0].ToString();					
						machine_id= data.Rows[0][3].ToString();	
						if(data.Rows[0][4] != null)
							period_time= Convert.ToInt16(data.Rows[0][4]);	
						if(data.Rows[0][5] != null)
							day= Convert.ToInt16(data.Rows[0][5]);			
						if(data.Rows[0][6] != null)
							month= Convert.ToInt16(data.Rows[0][6]);		
						if(data.Rows[0][7] != null)
							year= Convert.ToInt16(data.Rows[0][7]);			
						if(data.Rows[0][9] != null)
							status= Convert.ToInt16(data.Rows[0][9]);		
					}
				
				}
			}
			catch (MySqlException ex) 
			{
				error = "Error connecting to the server: " + ex.Message ;
			}
		}

	
		public void Confirm()
		{
			if((std_id != "") && (machine_id != ""))
			{
				string strCommandToExecute = "UPDATE `comp_reserve` SET `status` = 1 "+
					"WHERE `std_id` = '" + std_id + "'" +
					" AND `machine_id` = '"+ machine_id +"' AND `period_time` ="+ period_time +
					" AND `day` ="+ day +" AND `month` =" + month + " AND `year` =" + year;
				MySqlCommand cmdSQL = new MySqlCommand(strCommandToExecute,conn);
			
				cmdSQL.ExecuteNonQuery();
				status = 1;
			}
		}


		public int CheckTime(DateTime time)
		{
			DateTime now = time;
			if(std_id == "admin")
			{
				return 1;
			}
			else if((day == now.Day) && (month == now.Month) && (year == now.Year) &&
				(((period_time == now.Hour) && (now.Minute >= 30)) || 
				((period_time == (now.Hour - 1)) && (now.Minute < 30))))
			{
				if(comid == machine_id)
                	return 1;
				else
					return -1;
			}
			
			return 0;
		}


		private string GetMAC()
		{
			string macaddress = " `mac` = '00'";
			string tmp;
			ManagementObjectSearcher query = null;
			ManagementObjectCollection queryCollection = null;

			query = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapterConfiguration") ;

			queryCollection = query.Get();

			foreach( ManagementObject mo in queryCollection )
			{
				if(mo["MacAddress"] != null)
				{
					tmp = mo["MacAddress"].ToString();
					if(macaddress.IndexOf(tmp) == -1)
						macaddress += " or `mac` = '" + mo["MacAddress"].ToString() + "' ";
				}
			}
			return macaddress.Replace(":","-");
		}
	}

}

