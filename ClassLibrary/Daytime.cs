using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using MySql.Data.MySqlClient;
using System.Data;


//change namespace na kub
namespace ClassLibrary
{

	public class Daytime
	{
		private MySqlConnection		conn;
		private MySqlCommand		updatetime;
		private DataTable			data = new DataTable();
		private MySqlDataAdapter	da;
		private MySqlCommandBuilder	cb;

		private string connStr = String.Format("server={0};user id={1}; password={2}; pooling=false",
			DATABASE.ADDRESS, DATABASE.USERNAME, DATABASE.PASSWORD );  //Real Server
		//			"localhost", "root", "" );  //Localhost

		public DateTime GetTime()
		{
			DateTime result = new DateTime();
			if (conn != null)
				conn.Close();
	
			//			string connStr = String.Format("server={0};user id={1}; password={2}; database=mysql; pooling=false",
			
			
			//connect to database KITS
			conn = new MySqlConnection( connStr );
			conn.Open();
			conn.ChangeDatabase(DATABASE.TABLE);
			
			updatetime = new MySqlCommand("UPDATE `current_time` SET `time` = NOW( ) WHERE `id` =0",conn);
			updatetime.ExecuteNonQuery();
			
			
			
			da = new MySqlDataAdapter("SELECT * FROM `current_time` WHERE `id` =0",conn);
			
			cb = new MySqlCommandBuilder( da );
			da.Fill( data );
			
			result = Convert.ToDateTime(data.Rows[0][1]);
			return result;			
		}

		
	}
}
