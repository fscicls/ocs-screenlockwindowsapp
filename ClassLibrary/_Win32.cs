using System;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace ClassLibrary
{
	/// <summary>
	/// Summary description for _Win32.
	/// </summary>
	public class _Win32
	{
		private int KillApp(string ClsNm, string WndNm)
		{
			int i,j;
			i = User32.FindWindow(ClsNm ,WndNm);
			// Post a message to Calc to end its existence.
			if(i!=0)
			{
				j = User32.SendMessage(i, WM.SYSCOMMAND, SC.CLOSE, 0);
				return j;
			}
			else
				return -1;
		}

		public int KillTaskMng()
		{
			return KillApp("#32770","Windows Task Manager");
		}

		public void ShowThisFront(string ClsNm, string WndNm)
		{
			int hWnd = Win32.FindWindow(ClsNm ,WndNm);
			User32.ShowWindow(hWnd, SW.HIDE );
			User32.ShowWindow(hWnd, SW.SHOWNORMAL );
			User32.UpdateWindow(hWnd);
		}

		public int ShowTaskBar(bool bShowHide)
		{
            int hWnd = User32.FindWindow("Shell_TrayWnd", "");
			if (hWnd == 0)
				return 0;

			User32.ShowWindow(hWnd, bShowHide ? SW.SHOW : SW.HIDE);
			User32.UpdateWindow(hWnd);

			return 1;
		}

		public int DisableTaskMgr(bool bEnableDisable)
		{
			
			RegistryKey MyReg = Registry.CurrentUser.OpenSubKey
				("Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System",true);
			if(MyReg == null)
				MyReg = Registry.CurrentUser.CreateSubKey
					("Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System");

			if(bEnableDisable)
				MyReg.SetValue("DisableTaskMgr", 1);  // true disable TaskMgr
			else
				MyReg.SetValue("DisableTaskMgr", 0);  // false enable TaskMgr

			MyReg.Close();
			if(bEnableDisable)
				return 1;
			else
				return 0;
		}

		public _Win32()
		{
			
		}
	}
}
