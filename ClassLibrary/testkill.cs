using System;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace ClassLibrary
{
	/// <summary>
	/// Summary description for testkill.
	/// </summary>
	public class Win32
	{

		public const int WM_SYSCOMMAND  = 0x0112;
		public const int SC_CLOSE = 0xF060;
		
		[DllImport("user32.dll")]
		public static extern int FindWindow(
			string lpClassName,  // class name
			string lpWindowName  // window name
			);
		[DllImport("user32.dll")]
		public static extern int SendMessage(
			int hWnd,      // handle to destination window
			uint Msg,       // message
			int wParam,  // first message parameter
			int lParam   // second message parameter
			);
		[DllImport("user32.dll")] 
		public static extern int ShowWindow(
			 int hwnd, 
			 int nCmdShow
			 );
		[DllImport("user32.dll")]
		private static extern bool UpdateWindow(IntPtr hWnd);

	}
}
