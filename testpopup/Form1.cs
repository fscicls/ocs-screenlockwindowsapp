using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;

using CustomUIControls;

namespace testpopup
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class Form1 : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		
		TaskbarNotifier taskbarNotifier2;

		public Form1()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			taskbarNotifier2=new TaskbarNotifier();
			taskbarNotifier2.SetBackgroundBitmap(new Bitmap(GetType(),"skin2.bmp"),Color.FromArgb(255,0,255));
			taskbarNotifier2.SetCloseBitmap(new Bitmap(GetType(),"close2.bmp"),Color.FromArgb(255,0,255),new Point(300,74));
			taskbarNotifier2.TitleRectangle=new Rectangle(123,80,176,16);
			taskbarNotifier2.ContentRectangle=new Rectangle(116,97,197,22);
			taskbarNotifier2.TitleClick+=new EventHandler(TitleClick);
			taskbarNotifier2.ContentClick+=new EventHandler(ContentClick);
			taskbarNotifier2.CloseClick+=new EventHandler(CloseClick);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(64, 56);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(152, 32);
			this.button1.TabIndex = 0;
			this.button1.Text = "button1";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// Form1
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Controls.Add(this.button1);
			this.Name = "Form1";
			this.Text = "Form1";
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new Form1());
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
			taskbarNotifier2.CloseClickable = true;
			taskbarNotifier2.TitleClickable = true;
			taskbarNotifier2.ContentClickable = true;
			taskbarNotifier2.EnableSelectionRectangle = true;
			taskbarNotifier2.KeepVisibleOnMousOver = true;	// Added Rev 002
			taskbarNotifier2.ReShowOnMouseOver = true;			// Added Rev 002
			taskbarNotifier2.Show("��������������","��ҹ����������ա ...",500,3000,500);
		
		}
		void CloseClick(object obj,EventArgs ea)
		{
			MessageBox.Show("Closed was Clicked");
		}

		void TitleClick(object obj,EventArgs ea)
		{
			MessageBox.Show("Title was Clicked");
		}

		void ContentClick(object obj,EventArgs ea)
		{
			MessageBox.Show("Content was Clicked");
		}
	}
}
